const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");


router.post("/checkEmail",(req,res)=>{
	userControllers.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


router.post("/register",(req,res)=>{
	userControllers.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});


// ACTIVITY

router.post("/details",(req,res)=>{
	userControllers.getProfile(req.body).then(resultFromController=>res.send(resultFromController))
});

module.exports = router;